"use strict";

//const axios = require('axios');
/*
axios.post('/user', {
    firstName: 'Fred',
    lastName: 'Flintstone'
})
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    });
 */

const os = require('os');

class Config {
    constructor({debug = false} = {}) {
        this.debug = debug;
        this.base_dir = './';
    }

    /**
     * Singleton `i`nstance getter
     * @param {Object} [config]
     * @returns {Config}
     */
    static i(config = {}) {
        if (null === Config.prototype._i) {
            Config.prototype._i = new this(config);
        }
        return Config.prototype._i;
    }

    /**
     * Returns true when the environment is production, false when not
     *
     * @returns {boolean}
     */
    envProd() {
        return process.env.NODE_ENV === 'production';
    }

    /**
     * Config for this machine
     *
     * @returns {*}
     */
    main() {
        if (!this.envProd()) {
            try {
                // will end the if when successful, so main.json will not be used
                return require(this.base_dir + 'config/main.dev.json');
            } catch (e) {
                // Silently catch a non existing dev config
            }
        }
        return require(this.base_dir + 'config/main.json');
    }

    hostname() {
        return os.hostname;
    }

    /**
     * Default URLs for this machine
     *
     * @returns {{base: string}}
     */
    url() {
        return {
            base: 'http://' + (80 === this.main().port ? this.hostname() : this.hostname() + ':' + this.main().port)
        };
    }
}

/**
 * Will only be `undefined` on the first import
 */
if ('undefined' === typeof Config.prototype._i) {
    Config.prototype._i = null;
}

module.exports = Config;