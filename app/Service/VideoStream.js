"use strict";

const config = require('./Config');

const cors = require('cors');
const spawn = require('child_process').spawn;
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const crypto = require('crypto');

module.exports = class VideoStream {
    constructor({debug = false, encrypted = false} = {}) {
        this.encrypted = encrypted;

        // Camera stream options

        // Remember to use -hf and -vf to flip the image if required, like with raspistill
        this.raspividOptions = ['-o', '-', '-t', '0', '-vf', '-w', '960', '-h', '480', '-fps', '17'];
        this.ffmpegInputOptions = ['-re'];
        this.ffmpegOutputOptions = ['-vcodec copy', '-hls_flags delete_segments'];

        // Public directory that stores the stream files
        this.cameraDirectory = config.i().base_dir + config.i().main().stream.dir;

        // Create the camera output directory if it doesn't already exist
        // We don't want the async version since this only is run once at startup and the directory needs to be created
        // before we can really do anything else
        if (fs.existsSync(this.cameraDirectory) === false) {
            fs.mkdirSync(this.cameraDirectory);
        }
    }

    stream(app, express) {
        // Encrypt HLS stream?
        if (this.encrypted) {
            // Encryption files
            const keyFileName = 'enc.key';
            const keyInfoFileName = 'enc.keyinfo';

            // Setup encryption
            let keyFileContents = crypto.randomBytes(16);
            let initializationVector = crypto.randomBytes(16).toString('hex');
            let keyInfoFileContents = `${keyFileName}\n./${this.cameraDirectory}/${keyFileName}\n${initializationVector}`;

            // Populate the encryption files, overwrite them if necessary
            fs.writeFileSync(`./${this.cameraDirectory}/${keyFileName}`, keyFileContents);
            fs.writeFileSync(keyInfoFileName, keyInfoFileContents);

            // Add an option to the output stream to include the key info file in the livestream playlist
            this.ffmpegOutputOptions.push(`-hls_key_info_file ${keyInfoFileName}`);
        }

        // Start the camera stream
        let cameraStream = spawn('raspivid', this.raspividOptions);

        // Convert the camera stream to hls
        let conversion = new ffmpeg(cameraStream.stdout).noAudio().format('hls').inputOptions(this.ffmpegInputOptions).outputOptions(this.ffmpegOutputOptions).output(`${this.cameraDirectory}/${config.i().main().stream.playlist}`);

        // Set up stream conversion listeners
        conversion.on('error', function (err, stdout, stderr) {
            // console.log('Cannot process video: ' + err.message);
        });

        conversion.on('start', function (commandLine) {
            // console.log('Spawned Ffmpeg with command: ' + commandLine);
        });

        conversion.on('stderr', function (stderrLine) {
            // console.log('Stderr output: ' + stderrLine);
        });

        // Start the conversion
        conversion.run();

        // Allows CORS
        app.use(cors());

        // Set up a fileserver for the streaming video files
        app.use(`/${config.i().main().stream.dir}`, express.static(this.cameraDirectory));
    }
};