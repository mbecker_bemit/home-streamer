# Home Automation and Surveillance
## Raspberry PI Streamer, NodeJS

> This repository is not ready for others, only publishing my files without every config/database, in the hope they may help someone.

### Requirements

- NodeJS >8 (>10 rec.)
- Browser
- DHCP/DNS Server mapping hostnames
- a Raspberry PI running [Home: Base UI](https://bitbucket.org/mbecker_bemit/home-base-ui)

### Uses

- port `8080`
- hostnames, not tested with domains
- hostname recommended as `host` when calling in browser, see the `console.log` at the end
- a "few" NodeJS dependencies, especially `Express`, `Twig` and `heartbeats`


### Where is what

- `server.js` endpoint for the whole execution
- `/app`
- `/app/Controller` complexer full controller definitions
- `/app/Route` route initializer/simple controller dispatcher files
- `/app/Service` services classes
    - `Config.i()` singleton
    - `RouteCollection`
- `/asset` 
- `/asset/out` asset output folder mapped to `<host>:<port>/asset/out` <small>see `config/main.json`</small> 
- `/asset/out/hls.js` needed for support HLS (live streaming) in some browser, see: https://github.com/video-dev/hls.js/ 
- `/config` 
- `/config/main.example.json` config example for the base pi server <small>(delete `.example`)</small> 
- `/config/Streamer.example.json` config example for a pi camera server <small>(delete `.example`)</small> 
- `/System`
- `/System/Startup` startup/boilerplate for express, the app, twig and other things
- `/view` 
- `/view/*` default page_type template files, defining the end block model 
- `/view/*/**` other template files 
- `/view/View` the View Files

### Installation

Fetch it
```bash
git clone bitbucket.org:mbecker_bemit/home-base-ui.git
```

Install NodeJS dependencies, Composer is not needed at the moment, then start 

```bash
cd home-base-ui

npm i

# composer install

# starts as `node server.js`
npm start
# starts as `nodemon server.js`
npm run-script startmon
```