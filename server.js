const config = require('./app/Service/Config');

config.i({
    debug: !config.i().envProd()
}).base_dir = __dirname + '/';

/**
 * Execute the Startup
 *
 * @type {*}
 */
let run = require(config.i().base_dir + 'System/Startup');

//run.app.get('/', require(config.i().base_dir + 'app/Route/Home'));

const VideoStream = require('./app/Service/VideoStream');

let vs = new VideoStream({
    debug: true,
    // todo check config key, must be wrong so always `false`
    encrypted: config.encrypted
});
vs.stream(run.app, run.express);

run.app.get('/img', (req, res) => {

    res.append('X-Content-Type-Options', 'nosniff');
    res.append('Content-type', 'text/css');
    // todo
    // res.append('Last-Modified', '');
    res.render('Img', {
        url: url,
        data: data
    });
});

/**
 * Start Listening
 */

run.app.listen(config.i().main().port, () => {
    console.log('HLS camera stream at ' + config.i().url().base + '/' + config.i().main().stream.dir + '/' + config.i().main().stream.playlist);
    console.log('Server started on ' + config.i().url().base);
});
